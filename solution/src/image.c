//
// Created by vadim on 19.10.2023.
//

#include "image.h"
#include <malloc.h>


size_t row_size_bytes(struct image img) {
    size_t row_size = img.width * sizeof(struct pixel); // 3 bytes per pixel (without alignment)
    return row_size + padding(img.width);
}

size_t data_size_bytes(struct image img) {
    return row_size_bytes(img) * img.height; // Total data size = row size * number of rows
}

struct image create_image(uint64_t width, uint64_t height) {
    struct image img = { 0 };
    img.width = width;
    img.height = height;
    img.data = malloc(width * height * sizeof(struct pixel)); // Allocate memory for pixel data
    return img;
}

void destroy_image(struct image *img) {
    if (img && img->data) {
        free(img->data);
        img->data = NULL;
    }
}

size_t padding(size_t width) {
    return (4 - (sizeof(struct pixel) * width) % 4) % 4;
}
