//
// Created by vad1mchk on 18.10.2023.
//
#include "io_open_close.h"
#include <errno.h>

#define FILE_READ_BINARY_MODE "rb"
#define FILE_WRITE_BINARY_MODE "wb"

const char *open_status_messages[] = {
        [OPEN_OK] = "Opened successfully.",
        [OPEN_INPUT_FILE_NOT_FOUND] = "Input file does not exist or cannot be found.",
        [OPEN_INPUT_FILE_UNREADABLE] = "Cannot open input file for read (no read privileges).",
        [OPEN_INPUT_FILE_UNKNOWN_ERROR] = "An unknown error occurred when opening the input file.",
        [OPEN_OUTPUT_FILE_NOT_CREATED] = "Output file does not exist and cannot be created.",
        [OPEN_OUTPUT_FILE_UNWRITABLE] = "Cannot open output file for write (no write privileges).",
        [OPEN_OUTPUT_FILE_UNKNOWN_ERROR] = "An unknown error occurred when opening the output file."
};

const char *close_status_messages[] = {
        [CLOSE_OK] = "Closed successfully.",
        [CLOSE_INPUT_FILE_INVALID_POINTER] = "Invalid file pointer when closing input file.",
        [CLOSE_INPUT_FILE_UNKNOWN_ERROR] = "Unknown error occurred when closing input file.",
        [CLOSE_OUTPUT_FILE_INVALID_POINTER] = "Invalid file pointer when closing output file.",
        [CLOSE_OUTPUT_FILE_UNKNOWN_ERROR] = "Unknown error occurred when closing output file."
};

enum open_status open_input_file(const char *filename, FILE **file_to_open) {
    *file_to_open = fopen(filename, FILE_READ_BINARY_MODE);
    if (*file_to_open == NULL) {
        switch (errno) {
            case ENOENT:
                return OPEN_INPUT_FILE_NOT_FOUND;
            case EACCES:
                return OPEN_INPUT_FILE_UNREADABLE;
            default:
                return OPEN_INPUT_FILE_UNKNOWN_ERROR;
        }
    }
    return OPEN_OK;
}

enum open_status open_output_file(const char *filename, FILE **file_to_open) {
    *file_to_open = fopen(filename, FILE_WRITE_BINARY_MODE);
    if (*file_to_open == NULL) {
        switch (errno) {
            case EACCES:
                return OPEN_OUTPUT_FILE_UNWRITABLE;
            default:
                return OPEN_OUTPUT_FILE_UNKNOWN_ERROR;
        }
    }
    return OPEN_OK;
}

enum close_status close_input_file(FILE *file_to_close) {
    if (!file_to_close) {
        return CLOSE_INPUT_FILE_INVALID_POINTER;
    }
    if (fclose(file_to_close) == EOF) {
        return CLOSE_INPUT_FILE_UNKNOWN_ERROR;
    }
    return CLOSE_OK;
}

enum close_status close_output_file(FILE *file_to_close) {
    if (!file_to_close) {
        return CLOSE_OUTPUT_FILE_INVALID_POINTER;
    }
    if (fclose(file_to_close) == EOF) {
        return CLOSE_OUTPUT_FILE_UNKNOWN_ERROR;
    }
    return CLOSE_OK;
}
