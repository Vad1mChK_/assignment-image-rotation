#include "util.h"
#include "io_open_close.h"
#include "io_read_write.h"
#include "transform.h"
#include <stdio.h>

#define EXPECTED_ARGS_COUNT 3

int main(int argc, char **argv) {
    if (argc != 3) {
        print_err("[FATAL] ");
        printf("This program expects %d arguments, %d given.\n", EXPECTED_ARGS_COUNT - 1, argc - 1);
        return 1;
    }
    const char *input_filename = argv[1];
    const char *output_filename = argv[2];
    printf("Input file name: %s\n", input_filename);
    printf("Output file name: %s\n", output_filename);

    FILE *input_file, *output_file;
    struct image initial_img = {0}, transformed_img = {0};
    enum open_status input_open_status, output_open_status;
    enum read_status input_read_status;
    enum write_status output_write_status;
    enum close_status input_close_status, output_close_status;

    input_open_status = open_input_file(input_filename, &input_file);
    if (close_on_error(
            "OPEN INPUT FILE",
            input_open_status,
            open_status_messages,
            input_file,
            close_input_file)
            )
        return 1;

    input_read_status = read_from_bmp(input_file, &initial_img);
    if (close_on_error(
            "READ INPUT FILE",
            input_read_status,
            read_status_messages,
            input_file,
            close_input_file)
            ) {
        destroy_image(&initial_img);
        return 1;
    }


    input_close_status = close_input_file(input_file);
    if (close_on_error(
            "CLOSE INPUT FILE",
            input_close_status,
            close_status_messages,
            input_file,
            close_input_file)
            ) {
            destroy_image(&initial_img);
            return 1;
    }

    printf("Input image size: %zux%zu\n", initial_img.width, initial_img.height);
    transformed_img = create_image(initial_img.height, initial_img.width);
    rotate_90(&initial_img, &transformed_img);
    printf("Output image size: %zux%zu\n", transformed_img.width, transformed_img.height);
    destroy_image(&initial_img);

    output_open_status = open_output_file(output_filename, &output_file);
    if (close_on_error(
            "OPEN OUTPUT FILE",
            output_open_status,
            open_status_messages,
            output_file,
            close_output_file)
            ) {
        destroy_image(&transformed_img);
        return 1;
    }

    output_write_status = write_to_bmp(output_file, &transformed_img);
    if (close_on_error(
            "WRITE OUTPUT FILE",
            output_write_status,
            write_status_messages,
            output_file,
            close_output_file)
            ) {
        destroy_image(&transformed_img);
        return 1;
    }

    destroy_image(&transformed_img);

    output_close_status = close_output_file(output_file);
    if (close_on_error(
            "CLOSE OUTPUT FILE",
            output_close_status,
            close_status_messages,
            output_file,
            close_output_file)
            )
        return 1;

    return 0;
}
