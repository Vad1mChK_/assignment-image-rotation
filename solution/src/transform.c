//
// Created by vad1mchk on 19.10.2023.
//
#include "transform.h"
#include <malloc.h>

void rotate_90(struct image* const source, struct image* const destination) {
    size_t res_height = source->width;
    size_t res_width = source->height;

    destination->width = res_width;
    destination->height = res_height;

    for (size_t y = 0; y < res_height; ++y) {
        for (size_t x = 0; x < res_width; ++x) {
            destination->data[y * res_width + x] = source->data[(res_width - x - 1) * res_height + y];
        }
    }
}
