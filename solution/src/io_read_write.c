//
// Created by vad1mchk on 18.10.2023.
//
#include "bmp_header.h"
#include "image.h"
#include "io_read_write.h"
#include "util.h"
#include <malloc.h>

const char *read_status_messages[] = {
        [READ_OK] = "Read successfully.",
        [READ_INVALID_FILE_POINTER] = "The provided file pointer is NULL or invalid.",
        [READ_INVALID_SIGNATURE] = "The signature of the input file is invalid.",
        [READ_CORRUPT_HEADER] = "The BMP header of the input file is corrupt.",
        [READ_UNSUPPORTED_BIT_COUNT] = "The BMP bit count of the input file is unsupported (must be 24 bit).",
        [READ_CORRUPT_DATA] = "The BMP data in the input file is corrupt.",
        [READ_UNKNOWN_ERROR] = "An unknown error occurred when reading from the input file."
};

const char *write_status_messages[] = {
        [WRITE_OK] = "Written successfully.",
        [WRITE_INVALID_FILE_POINTER] = "The provided file pointer is NULL or invalid.",
        [WRITE_INVALID_IMAGE_POINTER] = "The provided image pointer is NULL or invalid.",
        [WRITE_HEADER_ERROR] = "An error has occurred when writing the BMP header.",
        [WRITE_DATA_ERROR] = "An error has occurred when writing the BMP data.",
        [WRITE_UNKNOWN_ERROR] = "An unknown error occurred when writing to the output file."
};

enum read_status read_from_bmp(FILE *file_from, struct image *img) {
    if (!file_from) {
        return READ_INVALID_FILE_POINTER;
    }

    struct bmp_header header;
    size_t header_read_size = fread(&header, sizeof(struct bmp_header), 1, file_from);
    if (header_read_size != 1) {
        return READ_CORRUPT_HEADER;
    }
    bmp_header_print(header);

    // Validate the BMP header
    if (
            (header.bf_type != DEFAULT_BF_TYPE_LE && header.bf_type != DEFAULT_BF_TYPE_BE) ||
            sizeof (header) != header.b_off_bits
    ) {
        return READ_INVALID_SIGNATURE;
    }
    if (header.bi_bit_count != DEFAULT_BI_BIT_COUNT) {
        return READ_UNSUPPORTED_BIT_COUNT;
    }

    *img = create_image(header.bi_width, header.bi_height);

    if (!img->data) {
        destroy_image(img);
        return READ_UNKNOWN_ERROR; // Memory allocation failure
    }

    if (fseek(file_from, (long) header.b_off_bits, SEEK_SET)) {
        destroy_image(img);
        return READ_UNKNOWN_ERROR;
    }

    size_t pad = padding(img->width);
    if (data_size_bytes(*img) > header.bi_size_image) {
        println_err("...");
    }

    // Read the BMP pixel data
    for (size_t y = 0; y < img->height; ++y) {
        // Read the pixel data for the current row
        size_t pixels_read_size = fread(
                &img->data[y * img->width],
                sizeof(struct pixel),
                img->width,
                file_from);
        if (pixels_read_size != img->width) {
            if (!feof(file_from)) {
                println_err("Unexpected EOF.");
            } else {
                fprintf(stderr, "Error in row y = %zu: expected to read %zu pixel(s), actual %zu.\n",
                        y,
                        img->width,
                        pixels_read_size
                );
                continue;
            }
            destroy_image(img);
            return READ_CORRUPT_DATA;
        }
        // Skip the padding bytes
        fseek(file_from, (long) pad, SEEK_CUR);
    }

    return READ_OK;
}

enum write_status write_to_bmp(FILE *file_to, const struct image *img) {
    if (!file_to) return WRITE_INVALID_FILE_POINTER;
    if (!img) return WRITE_INVALID_IMAGE_POINTER;

    struct bmp_header header = DEFAULT_HEADER;
    header.bfile_size = sizeof(header) + data_size_bytes(*img);
    header.bi_width = img->width;
    header.bi_height = img->height;
    header.bi_size_image = data_size_bytes(*img);
    bmp_header_print(header);

    uint8_t pads[3] = {0, 0, 0};
    size_t pad = padding(img->width);

    // Write header
    if (fwrite(&header, sizeof(header), 1, file_to) != 1) {
        return WRITE_HEADER_ERROR;
    }

    // Write pixel data
    for (size_t y = 0; y < img->height; ++y) {
        if (fwrite(img->data + y * img->width, sizeof(struct pixel), img->width, file_to) != img->width) {
            return WRITE_DATA_ERROR;
        }
        if (fwrite(pads, 1, pad, file_to) != pad) {
            return WRITE_DATA_ERROR;
        }
    }

    return WRITE_OK;
}
