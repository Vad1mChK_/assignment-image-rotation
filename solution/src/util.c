//
// Created by vad1mchk on 18.10.2023.
//

#include "util.h"
#include <stdio.h>

int print(const char *message) {
    return printf("%s", message);
}

int println(const char *message) {
    return printf("%s\n", message);
}

int print_err(const char *message) {
    return fprintf(stderr, "%s", message);
}

int println_err(const char *message) {
    return fprintf(stderr, "%s\n", message);
}

int close_on_error(
        const char *action,
        int status,
        const char **status_messages,
        FILE *file,
        enum close_status (*close_function)(FILE *)
) {
    if (status) { // If status of the action (regardless of the corresponding enum) is not 0, an error has occurred.
        fprintf(stderr, "[%s] %s\n", action, status_messages[status]);
        if (file) {
            int close_status = close_function(file);
            fprintf(stderr, "[CLOSE FILE] %s\n", close_status_messages[close_status]);
        }
        return 1;
    }
    printf("[%s] %s\n", action, status_messages[status]);
    return 0;
}
