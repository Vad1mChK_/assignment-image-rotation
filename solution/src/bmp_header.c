//
// Created by vad1mchk on 18.10.2023.
//

#include "bmp_header.h"
#include <stdio.h>

const struct bmp_header DEFAULT_HEADER = {
        .bf_type = DEFAULT_BF_TYPE_LE,
        // .bfile_size,
        .bf_reserved = DEFAULT_BF_RESERVED,
        .b_off_bits = DEFAULT_B_OFF_BITS,
        .bi_size = DEFAULT_BI_SIZE,
        // .bi_width,
        // .bi_height,
        .bi_planes = DEFAULT_BI_PLANES,
        .bi_bit_count = DEFAULT_BI_BIT_COUNT,
        .bi_compression = DEFAULT_BI_COMPRESSION,
        // .bi_size_image,
        .bi_x_pels_per_meter = DEFAULT_BI_PELS_PER_METER,
        .bi_y_pels_per_meter = DEFAULT_BI_PELS_PER_METER,
        .bi_clr_used = DEFAULT_BI_CLR_USED,
        .bi_clr_important = DEFAULT_BI_CLR_IMPORTANT
};

void bmp_header_print(struct bmp_header header) {
    printf("bmp_header (\n");
    printf("\tbfType: %" PRIu16 ",\n", header.bf_type);
    printf("\tbfileSize: %" PRIu32 ",\n", header.bfile_size);
    printf("\tbfReserved: %" PRIu32 ",\n", header.bf_reserved);
    printf("\tbOffBits: %" PRIu32 ",\n", header.b_off_bits);
    printf("\tbiSize: %" PRIu32 ",\n", header.bi_size);
    printf("\tbiWidth: %" PRIu32 ",\n", header.bi_width);
    printf("\tbiHeight: %" PRIu32 ",\n", header.bi_height);
    printf("\tbiPlanes: %" PRIu16 ",\n", header.bi_planes);
    printf("\tbiBitCount: %" PRIu16 ",\n", header.bi_bit_count);
    printf("\tbiCompression: %" PRIu32 ",\n", header.bi_compression);
    printf("\tbiSizeImage: %" PRIu32 ",\n", header.bi_size_image);
    printf("\tbiXPelsPerMeter: %" PRIu32 ",\n", header.bi_x_pels_per_meter);
    printf("\tbiYPelsPerMeter: %" PRIu32 ",\n", header.bi_y_pels_per_meter);
    printf("\tbiClrUsed: %" PRIu32 ",\n", header.bi_clr_used);
    printf("\tbiClrImportant: %" PRIu32 ",\n", header.bi_clr_important);
    printf(")\n");
}
