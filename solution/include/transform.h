//
// Created by vadim on 19.10.2023.
//

#ifndef IMAGE_TRANSFORMER_TRANSFORM_H
#define IMAGE_TRANSFORMER_TRANSFORM_H

#include "image.h"

void rotate_90(struct image* const source, struct image* const destination);

#endif //IMAGE_TRANSFORMER_TRANSFORM_H
