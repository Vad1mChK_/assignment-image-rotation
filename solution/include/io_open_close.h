//
// Created by vad1mchk on 18.10.2023.
//

#ifndef IMAGE_TRANSFORMER_IO_OPEN_CLOSE_H
#define IMAGE_TRANSFORMER_IO_OPEN_CLOSE_H

#include <stdio.h>

/**
 * Possible statuses when opening a file, indicating various problems or lack of such.
 */
enum open_status {
    /**
     * Indicates that the file was opened successfully.
     */
    OPEN_OK = 0,
    /**
     * Indicates that the input file does not exist or cannot be found.
     */
    OPEN_INPUT_FILE_NOT_FOUND,
    /**
     * Indicates that the input file cannot be opened for read (no read privileges).
     */
    OPEN_INPUT_FILE_UNREADABLE,
    /**
     * Indicates that there was an unknown error when opening the input file.
     */
    OPEN_INPUT_FILE_UNKNOWN_ERROR,
    /**
     * Indicates that the output file does not exist and cannot be created.
     */
    OPEN_OUTPUT_FILE_NOT_CREATED,
    /**
     * Indicates that the output file cannot be opened for write (no write privileges).
     */
    OPEN_OUTPUT_FILE_UNWRITABLE,
    /**
     * Indicates that there was an unknown error when opening the input file.
     */
    OPEN_OUTPUT_FILE_UNKNOWN_ERROR
};

/**
 * Messages to be used with the open_status enum values.
 */
extern const char *open_status_messages[];

/**
 * Opens the input file and assigns the opened file pointer to file_to_open.
 *
 * @param filename The name of the input file to open.
 * @param file_to_open The second-order pointer to the input file to open (changed by the function).
 * @return The open status.
 */
enum open_status open_input_file(const char *filename, FILE **file_to_open);

/**
 * Opens the output file and assigns the opened file pointer to file_to_open.
 *
 * @param filename The name of the output file to open.
 * @param file_to_open The second-order pointer to the output file to open (changed by the function).
 * @return The open status.
 */
enum open_status open_output_file(const char *filename, FILE **file_to_open);

/**
 * Possible statuses when closing a file, indicating various problems or lack of such.
 */
enum close_status {
    /**
     * Indicates that the file was closed successfully.
     */
    CLOSE_OK = 0,

    /**
     * Indicates an invalid file pointer was provided when trying to close the input file.
     */
    CLOSE_INPUT_FILE_INVALID_POINTER,

    /**
     * Indicates that an unknown error occurred when closing the input file.
     */
    CLOSE_INPUT_FILE_UNKNOWN_ERROR,

    /**
     * Indicates an invalid file pointer was provided when trying to close the output file.
     */
    CLOSE_OUTPUT_FILE_INVALID_POINTER,

    /**
     * Indicates that an unknown error occurred when closing the output file.
     */
    CLOSE_OUTPUT_FILE_UNKNOWN_ERROR,
};

/**
 * Messages to be used with the close_status enum values.
 */
extern const char *close_status_messages[];

/**
 * Closes the input file.
 * @param file_to_close The pointer to the input file to close.
 * @return The close status.
 */
enum close_status close_input_file(FILE *file_to_close);

/**
 * Closes the input file.
 * @param file_to_close The pointer to the output file to close.
 * @return The close status.
 */
enum close_status close_output_file(FILE *file_to_close);

#endif //IMAGE_TRANSFORMER_IO_OPEN_CLOSE_H
