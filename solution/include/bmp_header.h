//
// Created by vad1mchk on 2023/10/03.
//

#ifndef IMAGE_TRANSFORMER_BMP_HEADER_H
#define IMAGE_TRANSFORMER_BMP_HEADER_H

#include  <inttypes.h>

#define DEFAULT_BF_TYPE_LE 0x4D42
#define DEFAULT_BF_TYPE_BE 0x424D
#define DEFAULT_BF_RESERVED 0
#define DEFAULT_B_OFF_BITS 54
#define DEFAULT_BI_SIZE 40
#define DEFAULT_BI_BIT_COUNT 24
#define DEFAULT_BI_PLANES 1
#define DEFAULT_BI_COMPRESSION 0
#define DEFAULT_BI_PELS_PER_METER 2834
#define DEFAULT_BI_CLR_USED 0
#define DEFAULT_BI_CLR_IMPORTANT 0

#pragma pack(push, 1)
/**
 * Structure representing a BMP header.
 */
struct bmp_header { // Renamed the fields so that they adhere to snake_case naming convention
    uint16_t bf_type;
    uint32_t bfile_size;
    uint32_t bf_reserved;
    uint32_t b_off_bits;
    uint32_t bi_size;
    uint32_t bi_width;
    uint32_t bi_height;
    uint16_t bi_planes;
    uint16_t bi_bit_count;
    uint32_t bi_compression;
    uint32_t bi_size_image;
    uint32_t bi_x_pels_per_meter;
    uint32_t bi_y_pels_per_meter;
    uint32_t bi_clr_used;
    uint32_t bi_clr_important;
};
#pragma pack(pop)

/**
 * Represents a default BMP header, in which some of the fields are initialized with default values.
 * You can use it to create new headers by changing some fields to the values you need.
 */
extern const struct bmp_header DEFAULT_HEADER;

void bmp_header_print(struct bmp_header header);

#endif //IMAGE_TRANSFORMER_BMP_HEADER_H
