//
// Created by vad1mchk on 2023/10/03.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stddef.h>
#include <stdint.h>

/**
 * Structure that represents a 24-bit pixel with BGR (blue, green, red) channels;
 */
struct pixel {
    uint8_t b, g, r;
};

/**
 * Structure that represents an image with width, height, and data.
 */
struct image {
    size_t width, height;
    struct pixel *data;
};

/**
 * Calculates row size in bytes, taking into account the padding (the row size in bytes must be a multiple of 4).
 * @param img Image to extract the width from.
 * @return Row size in bytes.
 */
size_t row_size_bytes(struct image img);

/**
 * Calculates data size in bytes, taking into account the padding (the row size in bytes must be a multiple of 4).
 * @param img Image to extract the width and height from.
 * @return Data size in bytes.
 */
size_t data_size_bytes(struct image img);

/**
 * Creates a new image with the specified width and height and allocates memory for its data.
 * @param width Width of the image to create.
 * @param height Height of the image to create.
 * @return The newly created image.
 */
struct image create_image(size_t width, size_t height);

/**
 * Destroys the image and frees all its resources.
 * @param img The image to destroy.
 */
void destroy_image(struct image *img);

/**
 * Calculates the padding (trailing bytes count) of each row of an image with the specified width.
 * @param width Width of the image.
 * @return Padding bytes count.
 */
size_t padding(size_t width);

#endif //IMAGE_TRANSFORMER_IMAGE_H
