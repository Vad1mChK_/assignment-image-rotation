//
// Created by vad1mchk on 18.10.2023.
//

#ifndef IMAGE_TRANSFORMER_UTIL_H
#define IMAGE_TRANSFORMER_UTIL_H

#include "inttypes.h"
#include "io_open_close.h"

/**
 * Prints a message to the standard output (stdout).
 * @param message Message to print.
 * @return The result of the underlying printf call.
 */
int print(const char *message);

/**
 * Prints a message to the standard output (stdout) with a newline at the end.
 * @param message Message to print.
 * @return The result of the underlying printf call.
 */
int println(const char *message);

/**
 * Prints a message to the standard error stream (stderr).
 * @param message Message to print.
 * @return The result of the underlying fprintf call.
 */
int print_err(const char *message);

/**
 * Prints a message to the standard error stream (stderr) with a newline at the end.
 * @param message Message to print.
 * @return The result of the underlying fprintf call.
 */
int println_err(const char *message);

/**
 * If an error has occurred during `action`, prints an error message corresponding to `status` from the array
 * `status_messages` and closes the file `file` using `close_function`.
 * @param action The name of the action performed (e.g. "OPEN INPUT FILE")
 * @param status The status of the action.
 * @param status_messages The array with status messages.
 * @param file File to close in case of error.
 * @param close_function Function that closes the file.
 * @return 1 if an error has occurred, else 0.
 */
int close_on_error(
        const char *action,
        int status,
        const char **status_messages,
        FILE *file,
        enum close_status (*close_function)(FILE *)
);
#endif //IMAGE_TRANSFORMER_UTIL_H
