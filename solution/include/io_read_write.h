//
// Created by vad1mchk on 18.10.2023.
//

#ifndef IMAGE_TRANSFORMER_IO_READ_WRITE_H
#define IMAGE_TRANSFORMER_IO_READ_WRITE_H

#include "image.h"
#include <stdio.h>

/**
 * Possible statuses when reading data from a BMP file, indicating various problems or lack of such.
 */
enum read_status {
    /**
     * Indicates that the data was read successfully.
     */
    READ_OK = 0,
    /**
     * Indicates that the provided file pointer is invalid or null.
     */
    READ_INVALID_FILE_POINTER,
    /**
     * Indicates that the signature of the BMP file is invalid.
     */
    READ_INVALID_SIGNATURE,
    /**
     * Indicates that the BMP header was found to be corrupt.
     */
    READ_CORRUPT_HEADER,
    /**
     * Indicates that the bit count is unsupported (not 24 bit).
     */
    READ_UNSUPPORTED_BIT_COUNT,
    /**
     * Indicates that the data was found to be corrupt.
     */
    READ_CORRUPT_DATA,
    /**
     * Indicates that an unknown error has occurred when reading the file.
     */
    READ_UNKNOWN_ERROR
};

/**
 * Messages to be used with the read_status enum values.
 */
extern const char *read_status_messages[];

/**
 * Reads data from a BMP file into an image structure.
 *
 * @param file_from Pointer to the BMP file to be read.
 * @param img Pointer to the image structure where the BMP data will be stored.
 * @return Status of the reading operation as defined by the read_status enum.
 */
enum read_status read_from_bmp(FILE *file_from, struct image *img);

/**
 * Possible statuses when writing data to a BMP file, indicating various problems or lack of such.
 */
enum write_status {
    /**
     * Indicates that the data was written successfully.
     */
    WRITE_OK = 0,
    /**
     * Indicates that the provided file pointer is invalid or null.
     */
    WRITE_INVALID_FILE_POINTER,
    /**
     * Indicates that the provided image pointer is invalid or null.
     */
    WRITE_INVALID_IMAGE_POINTER,
    /**
     * Indicates that there was an issue writing the BMP header.
     */
    WRITE_HEADER_ERROR,
    /**
     * Indicates that there was an issue writing the BMP data.
     */
    WRITE_DATA_ERROR,
    /**
     * Indicates that an unknown error has occurred when writing the file.
     */
    WRITE_UNKNOWN_ERROR
};

/**
 * Messages to be used with the write_status enum values.
 */
extern const char *write_status_messages[];

/**
 * Writes image data to a BMP file.
 *
 * @param file_to Pointer to the BMP file where the data will be written.
 * @param img Pointer to the image structure containing the data to be written.
 * @return Status of the writing operation as defined by the write_status enum.
 */
enum write_status write_to_bmp(FILE *file_to, const struct image *img);

#endif //IMAGE_TRANSFORMER_IO_READ_WRITE_H
